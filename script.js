const userNumber= prompt("Введіть число");
if (isNaN(userNumber)) {
    alert("Введіть число!");
} else {
    const number = Number(userNumber);
    let found = false;
    for (let i = 1; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);
            found = true;
        }
    }
    if(!found) {
        console.log ("Sorry, no numbers");
    }
}